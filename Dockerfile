FROM golang:1.13 as builder

WORKDIR /go/src/github.com/sneak/xsum
COPY . .

RUN go get -v && make build

# this container doesn't do anything except hold the build artifact
# and make sure it compiles.

FROM alpine

COPY --from=builder /go/src/github.com/sneak/xsum/xsum /bin/xsum

CMD /bin/xsum

# FIXME add testing
