# xsum

This stores a file mtime, a size, and a cryptographically secure content checksum
in an xattr, so that you can verify that your files aren't corrupted
on filesystems that are dumb and don't include data checksums (e.g. apfs)

# inspired by 

* https://github.com/rfjakob/cshatag

# background

You can dd a few random bytes into the middle of an hfs+ or apfs filesystem,
and, if they land in file data, an fsck/Disk First Aid on the filesystem
will pass with flying colors.  There is no file content checksum.

# todo

* recurse into directories
* more parallelism

# author

Jeffrey Paul [sneak@sneak.berlin](mailto:sneak@sneak.berlin) 
