VERSION := $(shell git rev-parse --short HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildtime=$(BUILDTIME)
GOLDFLAGS += -X main.Builduser=$(BUILDUSER)@$(BUILDHOST)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)
GOFLAGS = -ldflags "$(GOLDFLAGS)"

default: rundebug

rundebug: build
	./xsum -v

run: build
	./xsum

build: ./xsum

./xsum: *.go
	go build -o $@ $(GOFLAGS) .

fmt:
	go fmt *.go

test:
	docker build -t sneak/xsum .
